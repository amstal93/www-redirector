FROM nginx:1-alpine

ARG WWW_REDIRECTOR_PATH=/opt/www-redirector
ENV WWW_REDIRECTOR_PATH=$WWW_REDIRECTOR_PATH

COPY nginx $WWW_REDIRECTOR_PATH/

COPY bash/init_container.sh $WWW_REDIRECTOR_PATH/
RUN chmod +x $WWW_REDIRECTOR_PATH/init_container.sh

CMD $WWW_REDIRECTOR_PATH/init_container.sh