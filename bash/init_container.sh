#!/bin/sh

set -e

envsubst '$WWW_REDIRECTOR_HOST' < $WWW_REDIRECTOR_PATH/$WWW_REDIRECTOR_MODE.conf > /etc/nginx/nginx.conf

nginx -g "daemon off;"
